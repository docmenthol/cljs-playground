(ns playground.app
  (:require [playground.datagrid :refer [datagrid]]
            [playground.sorting :refer [sorting]]
            [playground.lib.twc.core :as tw]))

(def columns
  [{:key :one   :display "One"}
   {:key :two   :display "Two"}
   {:key :three :display "Three"}
   {:key :four  :display "Four"}
   {:key :five  :display "Five"}])

(def cell-renderers
  {:one #(+ 13 %)})

(def data
  (vec
    (for [i (range 1 6)]
      (zipmap
        [:one :two :three :four :five]
        (for [j (range 1 6)] (* i j))))))

(defn app
  []
  [:div.flex.justify-center.pt-8.mx-64
   ; [tw/table
   ;  {:columns columns
   ;   :data data
   ;   ; :basic "very"
   ;   ; :basic true
   ;   :cell-class ["px-4" "py-2"]}]])
   ; [sorting {:items columns}]])
   [datagrid {:columns columns
              :data data
              :cell-renderers cell-renderers
              :draggable-columns true
              :editable true}]])

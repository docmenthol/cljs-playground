(ns playground.datagrid
  (:require [reagent.core :as r]
            [playground.lib.icons :refer [i-edit-pencil i-checkmark]]))

(def editing-rows
  (r/atom []))

(defn in?
  [coll val]
  (some #(= % val) coll))

(defn vec-remove
  [coll pos]
  (-> (subvec coll 0 pos)
      (concat (subvec coll (inc pos)))
      (vec)))

(defn do-thing
  [i]
  (let [new-thing (conj @editing-rows i)]
    (do (println new-thing)
        (reset! editing-rows new-thing))))

(defn handle-edit-row
  [row index]
  (swap! editing-rows vec-remove index))

(defn icon
  [props]
  (if (string? props)
    [:img.icon {:src (str "/icons/" props ".svg")}]
    [:img.icon {:src (str "/icons/" (:name props) ".svg") :class (:class props)}]))

(defn datagrid-header
  [{:keys [display]}]
  [:div.datagrid__header
    [:div display]])

(defn datagrid-row
  [{:keys [index data columns editable on-edit-row on-delete-row]}]
  (let [ks (map :key columns)]
    [:div.datagrid__row
     (for [k ks]
       [:div.datagrid__cell (get data k)])
     [:div.datagrid__cell
      (when editable
        (if-not (in? @editing-rows index)
          [:button.rounded.bg-gray-900.px-4.py-2
           {:on-click #(swap! editing-rows conj index)}
           [i-edit-pencil]]
          [:button.rounded.bg-gray-900.px-4.py-2
           {:on-click #(on-edit-row data index)}
           [i-checkmark]]))]]))

(defn datagrid
  "A datagrid component, one day."
  [{:keys [columns data cell-renderers editable edit-renderers on-edit-row on-delete-row]
    :or   {editable false cell-renderers {} edit-renderers {}}}]
  [:div.datagrid
   [:div.datagrid__row
     (for [[index c] (map-indexed vector columns)] ^{:key (str (name (:key c)) "-" index)}
       [datagrid-header {:display (:display c)}])
     [datagrid-header {:display "Actions"}]]
   (for [[index d] (map-indexed vector data)] ^{:key (str "row-" index)}
     [datagrid-row
      {:index index
       :data d
       :columns columns
       :editable editable
       :on-edit-row handle-edit-row}])])

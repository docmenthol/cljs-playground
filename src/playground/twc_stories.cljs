(ns playground.twc-stories
  (:require ["@storybook/react" :refer (storiesOf)]
            [reagent.core :as r]
            [playground.lib.twc.core :refer [table]]))

(def columns
  [{:key :one   :display "One"}
   {:key :two   :display "Two"}
   {:key :three :display "Three"}
   {:key :four  :display "Four"}
   {:key :five  :display "Five"}])

(def data
  (vec
    (for [i (range 1 6)]
      (zipmap
        [:one :two :three :four :five]
        (for [j (range 1 6)] (* i j))))))

(defn ^:export table-story
  []
  (let [c (r/as-component [table {:columns columns :data data}])]
    (do (println c)
        c)))

(-> (storiesOf "twc" js/module)
    (.add
      "twc.core.table"
      table-story))
      ; #(table {:columns columns :data data})))

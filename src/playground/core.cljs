(ns playground.core
  (:require [reagent.core :as r]
            [playground.app :refer [app]]))

(defn start []
  (r/render-component
    [app]
    (. js/document (getElementById "app"))))

(defn ^:export init
  []
  (start))

(defn stop
  []
  (js/console.log "stop"))

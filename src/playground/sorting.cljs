(ns playground.sorting
  (:require [reagent.core :as r]))

(def sorting-state (r/atom []))

(defn- halter
  [e]
  (.preventDefault e))

(defn- get-data
  [key e]
  (.getData (.. e -dataTransfer) key))

(defn- handle-drag-start
  [index]
  (fn [e]
    (halter e)
    (println "Drag start item" index)
    (.setData (.. e -dataTransfer) "index" index)
    (aset e "dataTransfer" "effectAllowed" "copy")))

(defn- handle-drag-over
  [e]
  (aset e "dataTransfer" "dropEffect" "copy")
  (let [what (get-data "index" e)]
    (println "Drag over" what)))

(defn- handle-drop
  [e]
  (let [what (get-data "index" e)]
    (println "Dropped on" what)))

(defn sortable-item
  [{:keys [key display]} index]
  [:div.flex-1.bg-gray-900.px-4.py-2.border.border-gray-700.select-none
    {:id key
     :draggable "true"
     :on-drag-start (handle-drag-start index)
     :on-drag-over handle-drag-over
     :on-drop handle-drop}
    display])

(defn sorting
  [{:keys [items order]
    :or {order nil}}]
  (r/create-class
    {:display-name "sortable"

     :component-did-mount
     (fn []
       (reset!
         sorting-state
         (or order
             (map first (map-indexed vector items)))))

     :reagent-render
     (fn [props]
       [:div.flex
        (for [index @sorting-state] ^{:key (str "item-" index)}
          [sortable-item (nth items index) index])])}))

(ns playground.lib.twc.core
  (:require [clojure.string :refer [join]]))

(defn table
  [{:keys [columns data table-class header-class row-class cell-class striped basic]}]
  [:div
   {:class
    (concat ["table"
             "rounded"
             (when-not (= basic "very") "border")
             "border-gray-700"
             "w-full"]
            table-class)}
   [:div.table-row {:class row-class}
    (for [[c-index c] (map-indexed vector columns)] ^{:key (join "-" ["header" (name (:key c))])}
      [:div
        {:class (concat ["table-cell"
                         "font-bold"
                         "border-b"
                         (when-not (or basic (= c-index (dec (count columns)))) "border-r")
                         "border-gray-700"
                         (when-not basic "bg-gray-900")]
                        cell-class
                        header-class)}
        (:display c)])]
   (for [[r-index d] (map-indexed vector data)] ^{:key (join "-" ["row" r-index])}
     [:div.table-row {:class (concat row-class [(when (and striped (odd? r-index)) "bg-gray-700")])}
      (for [c-index (range (count columns))] ^{:key (join "-" ["cell" r-index c-index])}
        (let [k (:key (nth columns c-index))]
          [:div
            {:class
             (concat
               ["table-cell"
                (when-not (= r-index (dec (count columns))) "border-b")
                (when-not (or basic (= c-index (dec (count columns)))) "border-r")
                "border-gray-700"]
               cell-class)}
            (get d k)]))])])

(ns dev.devcards
  (:require [reagent.core :refer [as-element]]
            [devtools.core :as devtools]
            [devcards.core :refer [start-devcard-ui!] :refer-macros [defcard]]
            [playground.lib.twc.core :refer [table]]))

(devtools/install!)
(enable-console-print!)

(def columns
  [{:key :one   :display "One"}
   {:key :two   :display "Two"}
   {:key :three :display "Three"}
   {:key :four  :display "Four"}
   {:key :five  :display "Five"}])

(def data
  (vec
    (for [i (range 1 6)]
      (zipmap
        [:one :two :three :four :five]
        (for [j (range 1 6)] (* i j))))))

(defcard tailwind-table
  (fn [] (as-element [table {:columns columns :data data}])))

(defn ^:export before-load
  []
  (println "start"))

(defn ^:export main
  []
  (start-devcard-ui!))
